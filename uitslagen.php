<?php
// Sessions, Security and Authorization
include ('security.php');

//Verbinding maken met de database
	require_once 'db.php';
	$mysqli = connectDB();
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>MBV Volley</title>
		<?php include 'head.html'
		?>
	</head>
	<body>
		<?php include 'header.php'
		?>
		<main class="container">
			<div class="well">
				<h1>Uitslagen</h1>
			</div>
			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="false">
				<?php 
					$sql = "SELECT * FROM SPEELWEEK";
					$resWeken = $mysqli->query($sql);
					if($resWeken->num_rows == 0) {
						echo '<div class="alert alert-warning" role="alert">'.
									'<i class="fa fa-exclamation-triangle"></i> Er zijn geen speelweken gevonden</div>';
					} else {
						$expanded = " in";
						while ($rowWeek = $resWeken->fetch_assoc()) { 
							$date = date("d F Y", strtotime($rowWeek['datum']));
							$panelID = 'heading'.$rowWeek['id'];
							$collapseID = 'collapse'.$rowWeek['id'];


				?>

				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="<?php echo $panelID ?>">
						<h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#<?php echo $collapseID ?>"
						aria-expanded="false" aria-controls="<?php echo $collapseID ?>"> Speelweek <?php echo $rowWeek['id'].": ".$date ?>
						</a></h4>
					</div>
					<div id="<?php echo $collapseID ?>" class="panel-collapse collapse<?php echo $expanded ?>" role="tabpanel"
					aria-labelledby="<?php echo $panelID ?>">
						<div class="panel-body">
							<?php
								$sql = "SELECT WVA.speelweek, WVA.datum, WVA.tijd, USTA.set, WVA.veld, WVA.klasse, USTA.punten AS 'punten A', USTA.score AS 'score A', WVA.teama, WVA.teamb, USTB.score AS 'score B', USTB.punten AS 'punten B' ".
										"FROM W_TEAM WTB, UITSLAG_SET_TEAM USTA, W_TEAM WTA, TEAM TA, WEDSTRIJD_VIEW WVA, UITSLAG_SET_TEAM USTB ".
										"WHERE WTA.rol=2 ".
										"AND USTA.wedstrijd=WTA.wedstrijd ".
										"AND USTA.wedstrijd=WTB.wedstrijd ".
										"AND USTB.wedstrijd=WTA.wedstrijd ".
										"AND USTB.wedstrijd=WTB.wedstrijd ".
										"AND USTA.wedstrijd = WVA.id ".
										"AND USTB.wedstrijd = WVA.id ".
										"AND USTA.wedstrijd=USTB.wedstrijd ".
										"AND USTA.set=USTB.set ".
										"AND WVA.uitslag = 1 ".
										"AND WTB.rol=1 ".
										"AND WTB.team=TA.id ".
										"AND USTA.team=WTB.team ".
										"AND USTB.team=WTA.team ".
										"AND taid !=0 ".
										"AND speelweek = '".$rowWeek['id']."' ";
								$resUitslagen = $mysqli->query($sql);
								if(!$resUitslagen || $resUitslagen->num_rows == 0) {
								echo '<div class="alert alert-info" role="alert">'.
								'<i class="fa fa-info-circle"></i> Er zijn geen uitslagen gevonden</div>';
								} else {
							?>
							<table class="table table-condensed table-striped">
								<tr>
									<th class="col-sm-1">Datum</th>
									<th class="col-sm-1">Tijd</th>
									<th class="col-sm-1">Klasse</th>
									<th class="col-sm-1">Set</th>
									<th class="col-sm-1">Punten A</th>
									<th class="col-sm-1">Score A</th>
									<th class="col-sm-2">Team A</th>
									<th class="col-sm-2">Team B</th>
									<th class="col-sm-1">Score B</th>
									<th class="col-sm-1">Punten B</th>
								</tr>
								<?php
								while ($rowUitslagen = $resUitslagen -> fetch_assoc()) {
									echo "<tr>";
									echo '<td>' . $rowUitslagen['datum'] . "</td>";
									echo "<td>" . $rowUitslagen['tijd'] . "</td>";
									echo "<td>" . $rowUitslagen['klasse'] . "</td>";
									echo "<td>" . $rowUitslagen['set'] . "</td>";
									echo "<td>" . $rowUitslagen['punten A'] . "</td>";
									echo "<td>" . $rowUitslagen['score A'] . "</td>";
									echo "<td>" . $rowUitslagen['teama'] . "</td>";
									echo "<td>" . $rowUitslagen['teamb'] . "</td>";
									echo "<td>" . $rowUitslagen['score B'] . "</td>";
									echo "<td>" . $rowUitslagen['punten B'] . "</td>";
									echo "</tr>";
								}
								?>
							</table>
							<?php } // end if ?>
						</div>
					</div>
				</div>

				<?php
					$expanded = "";
					}
				}
				?>
		</main>
	</body>
</html>
