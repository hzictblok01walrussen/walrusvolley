<?php
/**
 * Dit script is verantwoordelijk voor het lezen en schrijven van gebruikersgegevens. Er
 * is gekozen om dit met een JSON bestand te doen.
 */
 require_once 'db.php';
	 
	 /**
	  * Voegt een nieuwe gebruiker toe aan het gebruikersgegevensbestand.
	  * @param $username gebruikersnaam van de toe te voegen gebruiker
	  * @param $password wachtwoord van de toe te voegen gebruiker
	  */
	 function addUser($username, $password, $firstname, $lastname, $sex, $email, $telephone) {
		$mysqli = connectDB();
		
		if (isset($_POST['send'])) {
		$username = $_POST['username'];
		}
		$uservalues="INSERT INTO Users (Username)
		VALUE ('".$username."')";
		$result=$mysqli->query($uservalues);
	 }
	 
	 /**
	  * Authenticeert de gebruiker door meegestuurde logingegevens te vergelijken met
	  * het gebruikersgegevensbestand. Als gegevens overeenstemmen, dan worden enkele
	  * sessievariabelen geset (en eventueel een sessie gestart).
	  * @param $username te controleren gebruikersnaam
	  * @param $password te controleren wachtwoordgit
	  * @return TRUE als gebruiker is geauthentiseerd, FALSE als niet
	  */
	 function authenticateUser($username, $password) {
		$mysqli = connectDB();
		$uservalues="select username, password from users";
		$result=$mysqli->query($uservalues);
		if ($result->num_rows > 0){
			while ($resultarray = $result->fetch_assoc()) {
				$dbusername = $resultarray ['username'];
				$dbpassword = $resultarray['password'];
			
		
		if($dbusername == $username && $dbpassword == $password){
			if (!isset($_SESSION)) {
				session_start();
			}
			$_SESSION['loggedin'] = true;
			$_SESSION['username'] = $username;
			$_SESSION['role'] = $databaserole;
			return true;
		};
			};
		};
	 };
	 
	 /**
	  * Zorgt ervoor dat de gebruiker niet meer geaunthenticeerd is in deze sessie.
	  */
	 function unauthenticateUser() {
		unset($_SESSION['loggedin']);
		unset($_SESSION['username']);
		//session_destroy();
	 }
	 
	 /**
	  * Retourneert true als de opgegeven gebruikersnaam al bestaat
	  * @param de te controleren usernaam
	  * @return true als de opgegeven gebruikersnaam al bestaat
	  */
	 function usernameExists($username) {
		$mysqli = connectDB();
		// Selecteer username van de tabel users
	 	$sql = "SELECT Username FROM users WHERE Username='".$username."'";
		$result=$mysqli->query($sql);
		
		if ($result->num_rows > 0) {
			return true;
		}
		else {
			return false;
		}
	 }
	 
	 /**
	  * Retourneert true als de opgegeven gebruikersnaam al bestaat
	  * @param de te controleren usernaam
	  * @return true als de opgegeven gebruikersnaam al bestaat
	  */
	 function emailExists($email) {
	 	$userdata = readUsers();
		foreach ($userdata as $username => $user) {
			if($user['email']===$email)
				return true;
		}
		return false;	 	
	 }
	 
?>