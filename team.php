<?php
// Sessions, Security and Authorization
include ('security.php');

	
//Verbinding maken met de database
	require_once 'db.php';
	$mysqli =  connectDB();
	$teamid = 0;
	if(isset($_GET['teamid'])) {
		$teamid = $_GET['teamid'];
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>MBV Volley</title>
		<?php include 'head.html' ?>		
	</head>
	<body>
		<?php include 'header.php' ?>
		<main class="container">
		<?php
			$sql = "SELECT * FROM TEAM WHERE ID=".$teamid;
			$result = $mysqli->query($sql);
			if($result->num_rows >0) {
				$row = $result->fetch_assoc();
				$teamnaam = $row['naam'];
				echo '<div class="well"><h1>Team '. $teamid . ': '. $teamnaam .'</h1></div>';
		} ?>
		<div role="tabpanel">
			<!-- Nav tabs -->
			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation" class="active"><a href="#spelers" aria-controls="spelers" role="tab" data-toggle="tab">De spelers</a></li>
				<li role="presentation"><a href="#wedstrijden" aria-controls="wedstrijden" role="tab" data-toggle="tab">Wedstrijden</a></li>
				<li role="presentation"><a href="#uitslagen" aria-controls="uitslagen" role="tab" data-toggle="tab">Uitslagen</a></li>
				<li role="presentation"><a href="#statistieken" aria-controls="statistieken" role="tab" data-toggle="tab">Statistieken</a></li>
			</ul>

			<!-- Tab panes -->
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane fade in active" id="spelers">
					<?php // De spelers van dit team
					$sql = "SELECT L.naam FROM LID L, TEAM_HAS_LID TL WHERE L.id = TL.lid AND TL.team = ".$teamid;
					$resSpelers = $mysqli->query($sql);
					if($resSpelers->num_rows == 0 ) {
						echo '<div class="alert alert-warning" role="alert">'.
									'<i class="fa fa-exclamation-triangle"></i> Er zijn geen spelers in dit team</div>';
					} else { 
						echo '<table class="table table-striped">';
						while($rowSpeler = $resSpelers->fetch_assoc()) { ?>
							<tr>
								<td class="col-sm-1">
									<i class="fa fa-user fa-3x"></i>
								</td>
								<td class="col-sm-11">
									<strong><?php echo $rowSpeler['naam'] ?></strong><br/>
									<small>Al 3 seizoenen actief</small>
								</td>
							</tr>
						<?php } 
						echo "</table>";
					} ?>
				</div>
				<div role="tabpanel" class="tab-pane fade" id="wedstrijden">
					<?php // De wedstrijden van dit team
					$sql = "SELECT S.datum, W.tijd, W.veld, TB.naam ".
							"FROM W_TEAM WTA, WEDSTRIJD W, W_TEAM WTB, TEAM TB, SPEELWEEK S ".
							"WHERE WTA.team=".$teamid." AND WTA.Rol=1 AND WTA.wedstrijd=W.id ".
							"AND WTB.wedstrijd=W.id AND WTB.rol=2 AND WTB.team=TB.id ".
							"AND S.id=W.speelweek ".
							"UNION ".
							"SELECT S.datum, W.tijd, W.veld, TA.naam ".
							"FROM W_TEAM WTB, WEDSTRIJD W, W_TEAM WTA, TEAM TA, SPEELWEEK S ".
							"WHERE WTB.team=".$teamid." AND WTB.Rol=2 AND WTB.wedstrijd=W.id ".
							"AND WTA.wedstrijd=W.id AND WTA.rol=1 AND WTA.team=TA.id ".
							"AND S.id=W.speelweek ".
							"UNION ".
							"SELECT S.datum, W.tijd, W.veld, '<i>Scheidsrechter</i>' AS naam ".
							"FROM W_TEAM WTS, WEDSTRIJD W, SPEELWEEK S ".
							"WHERE WTS.team=".$teamid." AND WTS.Rol=3 ".
							"AND WTS.wedstrijd=W.id ".
							"AND S.id=W.speelweek ".
							"ORDER BY datum, tijd";
							//echo $sql."<br/>";
					$resWedstr = $mysqli->query($sql);
					if($resWedstr->num_rows == 0 ) {
						echo '<div class="alert alert-info" role="alert">'.
									'<i class="fa fa-info-circle"></i> Er zijn geen wedstrijden voor dit team</div>';
					} else { 
						?>  <table class="table table-striped">
								<tr>
									<th>Datum</th>
									<th>Tijd</th>
									<th>Veld</th>
									<th>Tegen</th>
								</tr>
							<?php	while($rowWedstrijd = $resWedstr->fetch_assoc()) { ?>
							<tr>
								<td class="col-sm-1">
									<?php echo $rowWedstrijd['datum'] ?><br/>
								</td>
								<td class="col-sm-1">
									<?php echo $rowWedstrijd['tijd'] ?><br/>
								</td>
								<td class="col-sm-1">
									<?php echo $rowWedstrijd['veld'] ?><br/>
								</td>
								<td class="col-sm-11">
									<strong><?php echo $rowWedstrijd['naam'] ?></strong><br/>
								</td>
							</tr>
						<?php } 
						echo "</table>";
					}?>
					</div>
				<div role="tabpanel" class="tab-pane fade" id="uitslagen">
					<?php // De uitslagen van dit team
					$sql = "SELECT WVA.speelweek, WVA.datum, WVA.tijd, USTA.set, WVA.veld, WVA.klasse, USTA.punten AS 'punten A', USTA.score AS 'score A', WVA.teama, WVA.teamb, USTB.score AS 'score B', USTB.punten AS 'punten B' ".
							"FROM W_TEAM WTB, UITSLAG_SET_TEAM USTA, W_TEAM WTA, TEAM TA, WEDSTRIJD_VIEW WVA, UITSLAG_SET_TEAM USTB ".
							"WHERE WTA.rol=2 ".
							"AND USTA.wedstrijd=WTA.wedstrijd ".
							"AND USTA.wedstrijd=WTB.wedstrijd ".
							"AND USTB.wedstrijd=WTA.wedstrijd ".
							"AND USTB.wedstrijd=WTB.wedstrijd ".
							"AND USTA.wedstrijd = WVA.id ".
							"AND USTB.wedstrijd = WVA.id ".
							"AND USTA.wedstrijd=USTB.wedstrijd ".
							"AND USTA.set=USTB.set ".
							"AND WVA.uitslag = 1 ".
							"AND WTB.rol=1 ".
							"AND WTB.team=TA.id ".
							"AND USTA.team=WTB.team ".
							"AND USTB.team=WTA.team ".
							"AND taid !=0 ".
							"AND WVA.taid='".$teamid."' ".
							"OR WTA.rol=2 ".
							"AND USTA.wedstrijd=WTA.wedstrijd ".
							"AND USTA.wedstrijd=WTB.wedstrijd ".
							"AND USTB.wedstrijd=WTA.wedstrijd ".
							"AND USTB.wedstrijd=WTB.wedstrijd ".
							"AND USTA.wedstrijd = WVA.id ".
							"AND USTB.wedstrijd = WVA.id ".
							"AND USTA.wedstrijd=USTB.wedstrijd ".
							"AND USTA.set=USTB.set ".
							"AND WVA.uitslag = 1 ".
							"AND WTB.rol=1 ".
							"AND WTB.team=TA.id ".
							"AND USTA.team=WTB.team ".
							"AND USTB.team=WTA.team ".
							"AND taid !=0 ".
							"AND WVA.tbid='".$teamid."' ";
							//echo $sql."<br/>";
					$resUitslagen = $mysqli->query($sql);
					if($resUitslagen->num_rows == 0 ) {
						echo '<div class="alert alert-info" role="alert">'.
									'<i class="fa fa-info-circle"></i> Er zijn geen uitslagen voor dit team</div>';
					} else { 
						?>  <table class="table table-striped">
								<tr>
									<th>Datum</th>
									<th>Tijd</th>
									<th>Set</th>
									<th>Punten A</th>
									<th>Score A</th>
									<th>Team A</th>
									<th>Team B</th>
									<th>Score B</th>
									<th>Punten B</th>
								</tr>
							<?php	while($rowUitslagen = $resUitslagen->fetch_assoc()) { ?>
							<tr>
								<td class="col-sm-1"><?php echo $rowUitslagen['datum'] ?><br/></td>
								<td class="col-sm-1"><?php echo $rowUitslagen['tijd'] ?><br/></td>
								<td class="col-sm-1"><?php echo $rowUitslagen['set'] ?><br/></td>
								<td class="col-sm-1"><?php echo $rowUitslagen['punten A'] ?><br/></td>
								<td class="col-sm-1"><?php echo $rowUitslagen['score A'] ?><br/></td>
								<td class="col-sm-2"><?php echo $rowUitslagen['teama'] ?><br/></td>
								<td class="col-sm-2"><?php echo $rowUitslagen['teamb'] ?><br/></td>
								<td class="col-sm-1"><?php echo $rowUitslagen['score B'] ?><br/></td>
								<td class="col-sm-1"><?php echo $rowUitslagen['punten B'] ?><br/></td>
							</tr>
						<?php } 
						echo "</table>";
					}?>
					</div>
				<div role="tabpanel" class="tab-pane fade" id="statistieken">
					<?php // De spelers van dit team
					$sql = "SELECT * FROM STATSVIEW WHERE team = ".$teamid;
					$resStats = $mysqli->query($sql);
					if(!$resStats || $resStats->num_rows == 0 ) {
						echo '<div class="alert alert-warning" role="alert">'.
									'<i class="fa fa-exclamation-triangle"></i> Er zijn geen statistieken in dit team</div>';
					} else { 
						$rowStats = $resStats->fetch_assoc();
						?> <table class="table table-striped">
							<tr>
								<td>
									<strong>W</strong><br/>
									<small><i>Aantal gespeelde wedstrijden</i></small>
								</td>
								<td>
									<strong><?php echo $rowStats['W'] ?></strong><br/>
								</td>
							</tr><tr>
								<td>
									<strong>P</strong><br/>
									<small><i>Competitiepunten (gewonnen sets min strafpunten)</i></small>
								</td>
								<td>
									<strong><?php echo $rowStats['P'] ?></strong><br/>
								</td>
							</tr><tr>
								<td>
									<strong>Sv</strong><br/>
									<small><i>Scores voor</i></small>
								</td>
								<td>
									<strong><?php echo $rowStats['Sv'] ?></strong><br/>
								</td>
							</tr><tr>
								<td>
									<strong>St</strong><br/>
									<small><i>Scores tegen</i></small>
								</td>
								<td>
									<strong><?php echo $rowStats['St'] ?></strong><br/>
								</td>
							</tr><tr>
								<td>
									<strong>S</strong><br/>
									<small><i>Score saldo (Sv - St)</i></small>
								</td>
								<td>
									<strong><?php echo $rowStats['S'] ?></strong><br/>
								</td>
							</tr><tr>
								<td>
									<strong>Str.P</strong><br/>
									<small><i>Strafpunten</i></small>
								</td>
								<td>
									<strong><?php echo $rowStats['strp'] ?></strong><br/>
								</td>
							</tr>
						</table>
					<?php } ?>
				</div>
			</div>

		</div>

		</main>
	</body>
</html>
 