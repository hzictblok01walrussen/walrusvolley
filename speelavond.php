<?php
// Sessions, Security and Authorization
include ('security.php');
	
	
//Verbinding maken met de database
	require_once 'db.php';
	$mysqli =  connectDB();
	$spelweek = 0;
	if(isset($_GET['spelweek'])) {
		$spelweek = $_GET['spelweek'];
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>MBV Volley</title>
		<?php include 'head.html' ?>		
	</head>
	<body>
		<?php include 'header.php' ?>
		<main class="container">	
			<div class="well"><h1>Wedstrijdschema spelweek <?php echo $spelweek ?></h1></div>
		</main>
	</body>
</html>
 