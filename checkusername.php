<?php
/*
 * Dit scriptje is het server deel van het stukje AJAX. Het enige dat dit moet doen
 * is de tekst 'true' of 'false' in de body van het http response te stoppen (in plaats
 * van HTML code in een 'normaal' php script).
 * 
 * Omdat ik ook hier gebruik maak van mijn 'library' voor het lezen en schrijven van
 * gebruikers (userstorage.php) van en naar een json bestandje, is dit met weinig code 
 * te realiseren. Sterker nog, als ik deze zin heb uitgetypt is het aantal regels
 * commentaar groter dan de feitelijke code.
 * 
 */
    include_once('userstorage.php');
	
	$username = $_GET['username'];
	
	if (usernameExists($username)) {
		echo "true";
	} else {
		echo "false";
	}
?>