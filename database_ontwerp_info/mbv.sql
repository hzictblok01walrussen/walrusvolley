-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server versie:                5.5.34 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Versie:              9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Databasestructuur van mbvolley wordt geschreven
DROP DATABASE IF EXISTS `mbvolley`;
CREATE DATABASE IF NOT EXISTS `mbvolley` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `mbvolley`;


-- Structuur van  tabel mbvolley.klas wordt geschreven
DROP TABLE IF EXISTS `klas`;
CREATE TABLE IF NOT EXISTS `klas` (
  `code` char(2) NOT NULL,
  `naam` varchar(50) NOT NULL DEFAULT '<naam>',
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel mbvolley.klas: ~5 rows (ongeveer)
/*!40000 ALTER TABLE `klas` DISABLE KEYS */;
INSERT INTO `klas` (`code`, `naam`) VALUES
	('D1', 'Dames 1'),
	('D2', 'Dames 2'),
	('H1', 'Heren 1'),
	('H2', 'Heren 2'),
	('H3', 'Heren 3');
/*!40000 ALTER TABLE `klas` ENABLE KEYS */;


-- Structuur van  tabel mbvolley.lid wordt geschreven
DROP TABLE IF EXISTS `lid`;
CREATE TABLE IF NOT EXISTS `lid` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `naam` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel mbvolley.lid: ~20 rows (ongeveer)
/*!40000 ALTER TABLE `lid` DISABLE KEYS */;
INSERT INTO `lid` (`id`, `naam`) VALUES
	(1, 'Crista Ahern'),
	(2, 'Lina Petrin'),
	(3, 'Larae Smallwood'),
	(4, 'Jackson Steger'),
	(5, 'Darcel Mccarville'),
	(6, 'Jenny Hyler'),
	(7, 'Mable Harrod'),
	(8, 'Aleida Buff'),
	(9, 'Bailey Eisenhower'),
	(10, 'Mose Campion'),
	(11, 'Shae Pfannenstiel'),
	(12, 'Reid Bugbee'),
	(13, 'Emmitt Frye'),
	(14, 'Danielle Digennaro'),
	(15, 'Gertrud Schalk'),
	(16, 'Ha Halpern'),
	(17, 'Marica Bess'),
	(18, 'Manuel Reader'),
	(19, 'Zella Cloe'),
	(20, 'Reinaldo Shuffler');
/*!40000 ALTER TABLE `lid` ENABLE KEYS */;


-- Structuur van  tabel mbvolley.speelweek wordt geschreven
DROP TABLE IF EXISTS `speelweek`;
CREATE TABLE IF NOT EXISTS `speelweek` (
  `id` int(11) NOT NULL,
  `datum` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel mbvolley.speelweek: ~36 rows (ongeveer)
/*!40000 ALTER TABLE `speelweek` DISABLE KEYS */;
INSERT INTO `speelweek` (`id`, `datum`) VALUES
	(1, '2014-09-09'),
	(2, '2014-09-16'),
	(3, '2014-09-23'),
	(4, '2014-09-30'),
	(5, '2014-10-07'),
	(6, '2014-10-14'),
	(7, '2014-10-21'),
	(8, '2014-10-28'),
	(9, '2014-11-04'),
	(10, '2014-11-11'),
	(11, '2014-11-18'),
	(12, '2014-11-25'),
	(13, '2014-12-02'),
	(14, '2014-12-09'),
	(15, '2014-12-16'),
	(16, '2015-01-06'),
	(17, '2015-01-13'),
	(18, '2015-01-20'),
	(19, '2015-01-27'),
	(20, '2015-02-03'),
	(21, '2015-02-10'),
	(22, '2015-02-17'),
	(23, '2015-02-24'),
	(24, '2015-03-03'),
	(25, '2015-03-10'),
	(26, '2015-03-17'),
	(27, '2015-03-24'),
	(28, '2015-03-31'),
	(29, '2015-04-07'),
	(30, '2015-04-14'),
	(31, '2015-04-21'),
	(32, '2015-04-28'),
	(33, '2015-05-05'),
	(34, '2015-05-12'),
	(35, '2015-05-19'),
	(36, '2015-05-26');
/*!40000 ALTER TABLE `speelweek` ENABLE KEYS */;


-- Structuur van  view mbvolley.statsview wordt geschreven
DROP VIEW IF EXISTS `statsview`;
-- Tijdelijke tabel wordt aangemaakt zodat we geen VIEW afhankelijkheidsfouten krijgen
CREATE TABLE `statsview` (
	`klasse` CHAR(2) NOT NULL COLLATE 'latin1_swedish_ci',
	`team` INT(11) NOT NULL,
	`naam` VARCHAR(64) NOT NULL COLLATE 'latin1_swedish_ci',
	`W` BIGINT(21) NOT NULL,
	`P` DECIMAL(33,0) NULL,
	`Sv` DECIMAL(32,0) NULL,
	`St` DECIMAL(32,0) NULL,
	`S` DECIMAL(33,0) NULL,
	`strp` DECIMAL(32,0) NULL
) ENGINE=MyISAM;


-- Structuur van  tabel mbvolley.team wordt geschreven
DROP TABLE IF EXISTS `team`;
CREATE TABLE IF NOT EXISTS `team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `klasse` char(2) NOT NULL,
  `naam` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_team_klas_idx` (`klasse`),
  CONSTRAINT `fk_team_klas` FOREIGN KEY (`klasse`) REFERENCES `klas` (`code`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel mbvolley.team: ~9 rows (ongeveer)
/*!40000 ALTER TABLE `team` DISABLE KEYS */;
INSERT INTO `team` (`id`, `klasse`, `naam`) VALUES
	(1, 'H1', 'Munter Volley 1'),
	(2, 'H1', 'De Graaf Assurantien'),
	(3, 'H1', 'Sport2000 Mdb/\'t Znd'),
	(4, 'H3', 'SMA Zeeland'),
	(5, 'H3', 'Bowling d Kruitmolen'),
	(6, 'H2', 'Munter Volley 2'),
	(7, 'D1', 'Vlijtig Liesje'),
	(8, 'D1', 'Res. Valkenisse'),
	(9, 'D1', 'The Tube');
/*!40000 ALTER TABLE `team` ENABLE KEYS */;


-- Structuur van  tabel mbvolley.team_has_lid wordt geschreven
DROP TABLE IF EXISTS `team_has_lid`;
CREATE TABLE IF NOT EXISTS `team_has_lid` (
  `team` int(11) NOT NULL,
  `lid` int(11) NOT NULL,
  PRIMARY KEY (`team`,`lid`),
  KEY `fk_team_has_lid_lid1_idx` (`lid`),
  KEY `fk_team_has_lid_team1_idx` (`team`),
  CONSTRAINT `fk_team_has_lid_lid1` FOREIGN KEY (`lid`) REFERENCES `lid` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_team_has_lid_team1` FOREIGN KEY (`team`) REFERENCES `team` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel mbvolley.team_has_lid: ~69 rows (ongeveer)
/*!40000 ALTER TABLE `team_has_lid` DISABLE KEYS */;
INSERT INTO `team_has_lid` (`team`, `lid`) VALUES
	(1, 1),
	(3, 1),
	(6, 1),
	(8, 1),
	(1, 2),
	(3, 2),
	(6, 2),
	(8, 2),
	(1, 3),
	(4, 3),
	(6, 3),
	(9, 3),
	(1, 4),
	(4, 4),
	(6, 4),
	(9, 4),
	(1, 5),
	(4, 5),
	(6, 5),
	(9, 5),
	(1, 6),
	(4, 6),
	(6, 6),
	(9, 6),
	(1, 7),
	(4, 7),
	(7, 7),
	(9, 7),
	(2, 8),
	(4, 8),
	(7, 8),
	(9, 8),
	(2, 9),
	(4, 9),
	(7, 9),
	(9, 9),
	(2, 10),
	(4, 10),
	(7, 10),
	(2, 11),
	(4, 11),
	(7, 11),
	(2, 12),
	(5, 12),
	(7, 12),
	(2, 13),
	(5, 13),
	(8, 13),
	(2, 14),
	(5, 14),
	(8, 14),
	(2, 15),
	(5, 15),
	(8, 15),
	(3, 16),
	(5, 16),
	(8, 16),
	(3, 17),
	(5, 17),
	(8, 17),
	(3, 18),
	(5, 18),
	(8, 18),
	(3, 19),
	(6, 19),
	(8, 19),
	(3, 20),
	(6, 20),
	(8, 20);
/*!40000 ALTER TABLE `team_has_lid` ENABLE KEYS */;


-- Structuur van  view mbvolley.team_strp wordt geschreven
DROP VIEW IF EXISTS `team_strp`;
-- Tijdelijke tabel wordt aangemaakt zodat we geen VIEW afhankelijkheidsfouten krijgen
CREATE TABLE `team_strp` (
	`id` INT(11) NOT NULL,
	`klasse` CHAR(2) NOT NULL COLLATE 'latin1_swedish_ci',
	`naam` VARCHAR(64) NOT NULL COLLATE 'latin1_swedish_ci',
	`Strp` DECIMAL(32,0) NULL
) ENGINE=MyISAM;


-- Structuur van  view mbvolley.team_uitslag_view wordt geschreven
DROP VIEW IF EXISTS `team_uitslag_view`;
-- Tijdelijke tabel wordt aangemaakt zodat we geen VIEW afhankelijkheidsfouten krijgen
CREATE TABLE `team_uitslag_view` (
	`id` INT(11) NOT NULL,
	`klasse` CHAR(2) NOT NULL COLLATE 'latin1_swedish_ci',
	`naam` VARCHAR(64) NOT NULL COLLATE 'latin1_swedish_ci',
	`Strp` DECIMAL(32,0) NULL
) ENGINE=MyISAM;


-- Structuur van  tabel mbvolley.uitslag_set_team wordt geschreven
DROP TABLE IF EXISTS `uitslag_set_team`;
CREATE TABLE IF NOT EXISTS `uitslag_set_team` (
  `wedstrijd` int(11) NOT NULL,
  `set` int(11) NOT NULL,
  `team` int(11) NOT NULL,
  `score` int(11) NOT NULL,
  `punten` int(11) NOT NULL,
  PRIMARY KEY (`wedstrijd`,`set`,`team`),
  KEY `fk_SET_has_WEDSTRIJD_has_team_WEDSTRIJD_has_team1_idx` (`wedstrijd`,`team`),
  CONSTRAINT `fk_SET_has_WEDSTRIJD_has_team_WEDSTRIJD_has_team1` FOREIGN KEY (`wedstrijd`, `team`) REFERENCES `w_team` (`wedstrijd`, `team`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel mbvolley.uitslag_set_team: ~16 rows (ongeveer)
/*!40000 ALTER TABLE `uitslag_set_team` DISABLE KEYS */;
INSERT INTO `uitslag_set_team` (`wedstrijd`, `set`, `team`, `score`, `punten`) VALUES
	(1, 1, 7, 18, 0),
	(1, 1, 8, 25, 2),
	(1, 2, 7, 11, 0),
	(1, 2, 8, 25, 2),
	(1, 3, 7, 23, 0),
	(1, 3, 8, 25, 2),
	(1, 4, 7, 12, 2),
	(1, 4, 8, 4, 0),
	(2, 1, 1, 25, 2),
	(2, 1, 2, 11, 0),
	(2, 2, 1, 25, 2),
	(2, 2, 2, 3, 0),
	(2, 3, 1, 25, 2),
	(2, 3, 2, 23, 0),
	(2, 4, 1, 12, 0),
	(2, 4, 2, 25, 2);
/*!40000 ALTER TABLE `uitslag_set_team` ENABLE KEYS */;


-- Structuur van  tabel mbvolley.wedstrijd wordt geschreven
DROP TABLE IF EXISTS `wedstrijd`;
CREATE TABLE IF NOT EXISTS `wedstrijd` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `speelweek` int(11) NOT NULL,
  `tijd` time NOT NULL,
  `veld` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_wedstrijd_speelweek` (`speelweek`),
  CONSTRAINT `FK_wedstrijd_speelweek` FOREIGN KEY (`speelweek`) REFERENCES `speelweek` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=180 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel mbvolley.wedstrijd: ~179 rows (ongeveer)
/*!40000 ALTER TABLE `wedstrijd` DISABLE KEYS */;
INSERT INTO `wedstrijd` (`id`, `speelweek`, `tijd`, `veld`) VALUES
	(1, 1, '19:15:00', 2),
	(2, 1, '19:15:00', 3),
	(3, 1, '19:15:00', 4),
	(4, 1, '19:15:00', 5),
	(5, 1, '19:15:00', 6),
	(6, 1, '20:15:00', 1),
	(7, 1, '20:15:00', 2),
	(8, 1, '20:15:00', 3),
	(9, 1, '20:15:00', 4),
	(10, 1, '20:15:00', 5),
	(11, 1, '20:15:00', 6),
	(12, 1, '21:15:00', 1),
	(13, 1, '21:15:00', 2),
	(14, 1, '21:15:00', 3),
	(15, 1, '21:15:00', 4),
	(16, 1, '21:15:00', 5),
	(17, 2, '21:15:00', 6),
	(18, 2, '19:15:00', 1),
	(19, 2, '19:15:00', 2),
	(20, 2, '19:15:00', 3),
	(21, 2, '19:15:00', 4),
	(22, 2, '19:15:00', 5),
	(23, 2, '19:15:00', 6),
	(24, 2, '20:15:00', 1),
	(25, 2, '20:15:00', 2),
	(26, 2, '20:15:00', 3),
	(27, 2, '20:15:00', 4),
	(28, 2, '20:15:00', 5),
	(29, 2, '20:15:00', 6),
	(30, 2, '21:15:00', 1),
	(31, 2, '21:15:00', 2),
	(32, 2, '21:15:00', 3),
	(33, 2, '21:15:00', 4),
	(34, 2, '21:15:00', 5),
	(35, 2, '21:15:00', 6),
	(36, 3, '19:15:00', 1),
	(37, 3, '19:15:00', 2),
	(38, 3, '19:15:00', 3),
	(39, 3, '19:15:00', 4),
	(40, 3, '19:15:00', 5),
	(41, 3, '19:15:00', 6),
	(42, 3, '20:15:00', 1),
	(43, 3, '20:15:00', 2),
	(44, 3, '20:15:00', 3),
	(45, 3, '20:15:00', 4),
	(46, 3, '20:15:00', 5),
	(47, 3, '20:15:00', 6),
	(48, 3, '21:15:00', 1),
	(49, 3, '21:15:00', 2),
	(50, 3, '21:15:00', 3),
	(51, 3, '21:15:00', 4),
	(52, 3, '21:15:00', 5),
	(53, 3, '21:15:00', 6),
	(54, 4, '19:15:00', 1),
	(55, 4, '19:15:00', 2),
	(56, 4, '19:15:00', 3),
	(57, 4, '19:15:00', 4),
	(58, 4, '19:15:00', 5),
	(59, 4, '19:15:00', 6),
	(60, 4, '20:15:00', 1),
	(61, 4, '20:15:00', 2),
	(62, 4, '20:15:00', 3),
	(63, 4, '20:15:00', 4),
	(64, 4, '20:15:00', 5),
	(65, 4, '20:15:00', 6),
	(66, 4, '21:15:00', 1),
	(67, 4, '21:15:00', 2),
	(68, 4, '21:15:00', 3),
	(69, 4, '21:15:00', 4),
	(70, 4, '21:15:00', 5),
	(71, 4, '21:15:00', 6),
	(72, 5, '19:15:00', 1),
	(73, 5, '19:15:00', 2),
	(74, 5, '19:15:00', 3),
	(75, 5, '19:15:00', 4),
	(76, 5, '19:15:00', 5),
	(77, 5, '19:15:00', 6),
	(78, 5, '20:15:00', 1),
	(79, 5, '20:15:00', 2),
	(80, 5, '20:15:00', 3),
	(81, 5, '20:15:00', 4),
	(82, 5, '20:15:00', 5),
	(83, 5, '20:15:00', 6),
	(84, 5, '21:15:00', 1),
	(85, 5, '21:15:00', 2),
	(86, 5, '21:15:00', 3),
	(87, 5, '21:15:00', 4),
	(88, 5, '21:15:00', 5),
	(89, 5, '21:15:00', 6),
	(90, 6, '19:15:00', 1),
	(91, 6, '19:15:00', 2),
	(92, 6, '19:15:00', 3),
	(93, 6, '19:15:00', 4),
	(94, 6, '19:15:00', 5),
	(95, 6, '19:15:00', 6),
	(96, 6, '20:15:00', 1),
	(97, 6, '20:15:00', 2),
	(98, 6, '20:15:00', 3),
	(99, 6, '20:15:00', 4),
	(100, 6, '20:15:00', 5),
	(101, 6, '20:15:00', 6),
	(102, 6, '21:15:00', 1),
	(103, 6, '21:15:00', 2),
	(104, 6, '21:15:00', 3),
	(105, 6, '21:15:00', 4),
	(106, 6, '21:15:00', 5),
	(107, 6, '21:15:00', 6),
	(108, 7, '19:15:00', 1),
	(109, 7, '19:15:00', 2),
	(110, 7, '19:15:00', 3),
	(111, 7, '19:15:00', 4),
	(112, 7, '19:15:00', 5),
	(113, 7, '19:15:00', 6),
	(114, 7, '20:15:00', 1),
	(115, 7, '20:15:00', 2),
	(116, 7, '20:15:00', 3),
	(117, 7, '20:15:00', 4),
	(118, 7, '20:15:00', 5),
	(119, 7, '20:15:00', 6),
	(120, 7, '21:15:00', 1),
	(121, 7, '21:15:00', 2),
	(122, 7, '21:15:00', 3),
	(123, 7, '21:15:00', 4),
	(124, 7, '21:15:00', 5),
	(125, 7, '21:15:00', 6),
	(126, 8, '19:15:00', 1),
	(127, 8, '19:15:00', 2),
	(128, 8, '19:15:00', 3),
	(129, 8, '19:15:00', 4),
	(130, 8, '19:15:00', 5),
	(131, 8, '19:15:00', 6),
	(132, 8, '20:15:00', 1),
	(133, 8, '20:15:00', 2),
	(134, 8, '20:15:00', 3),
	(135, 8, '20:15:00', 4),
	(136, 8, '20:15:00', 5),
	(137, 8, '20:15:00', 6),
	(138, 8, '21:15:00', 1),
	(139, 8, '21:15:00', 2),
	(140, 8, '21:15:00', 3),
	(141, 8, '21:15:00', 4),
	(142, 8, '21:15:00', 5),
	(143, 8, '21:15:00', 6),
	(144, 9, '19:15:00', 1),
	(145, 9, '19:15:00', 2),
	(146, 9, '19:15:00', 3),
	(147, 9, '19:15:00', 4),
	(148, 9, '19:15:00', 5),
	(149, 9, '19:15:00', 6),
	(150, 9, '20:15:00', 1),
	(151, 9, '20:15:00', 2),
	(152, 9, '20:15:00', 3),
	(153, 9, '20:15:00', 4),
	(154, 9, '20:15:00', 5),
	(155, 9, '20:15:00', 6),
	(156, 9, '21:15:00', 1),
	(157, 9, '21:15:00', 2),
	(158, 9, '21:15:00', 3),
	(159, 9, '21:15:00', 4),
	(160, 9, '21:15:00', 5),
	(161, 9, '21:15:00', 6),
	(162, 9, '19:15:00', 1),
	(163, 10, '19:15:00', 2),
	(164, 10, '19:15:00', 3),
	(165, 10, '19:15:00', 4),
	(166, 10, '19:15:00', 5),
	(167, 10, '19:15:00', 6),
	(168, 10, '20:15:00', 1),
	(169, 10, '20:15:00', 2),
	(170, 10, '20:15:00', 3),
	(171, 10, '20:15:00', 4),
	(172, 10, '20:15:00', 5),
	(173, 10, '20:15:00', 6),
	(174, 10, '21:15:00', 1),
	(175, 10, '21:15:00', 2),
	(176, 10, '21:15:00', 3),
	(177, 10, '21:15:00', 4),
	(178, 10, '21:15:00', 5),
	(179, 10, '21:15:00', 6);
/*!40000 ALTER TABLE `wedstrijd` ENABLE KEYS */;


-- Structuur van  view mbvolley.wedstrijdschema_view wordt geschreven
DROP VIEW IF EXISTS `wedstrijdschema_view`;
-- Tijdelijke tabel wordt aangemaakt zodat we geen VIEW afhankelijkheidsfouten krijgen
CREATE TABLE `wedstrijdschema_view` (
	`id` INT(11) NOT NULL,
	`speelweek` INT(11) NOT NULL,
	`datum` DATE NULL,
	`tijd` TIME NOT NULL,
	`veld` INT(11) NOT NULL,
	`klasse` VARCHAR(2) NOT NULL COLLATE 'latin1_swedish_ci',
	`taid` BIGINT(20) NOT NULL,
	`teama` VARCHAR(64) NOT NULL COLLATE 'latin1_swedish_ci',
	`tbid` BIGINT(20) NOT NULL,
	`teamb` VARCHAR(64) NOT NULL COLLATE 'latin1_swedish_ci',
	`teams` VARCHAR(64) NOT NULL COLLATE 'latin1_swedish_ci',
	`klasse2` VARCHAR(2) NOT NULL COLLATE 'latin1_swedish_ci',
	`uitslag` BIGINT(20) NOT NULL
) ENGINE=MyISAM;


-- Structuur van  view mbvolley.wedstrijd_view wordt geschreven
DROP VIEW IF EXISTS `wedstrijd_view`;
-- Tijdelijke tabel wordt aangemaakt zodat we geen VIEW afhankelijkheidsfouten krijgen
CREATE TABLE `wedstrijd_view` (
	`id` INT(11) NOT NULL,
	`speelweek` INT(11) NOT NULL,
	`datum` DATE NULL,
	`tijd` TIME NOT NULL,
	`veld` INT(11) NOT NULL,
	`klasse` CHAR(2) NOT NULL COLLATE 'latin1_swedish_ci',
	`taid` INT(11) NOT NULL,
	`teama` VARCHAR(64) NOT NULL COLLATE 'latin1_swedish_ci',
	`tbid` INT(11) NOT NULL,
	`teamb` VARCHAR(64) NOT NULL COLLATE 'latin1_swedish_ci',
	`teams` VARCHAR(64) NOT NULL COLLATE 'latin1_swedish_ci',
	`klasse2` CHAR(2) NOT NULL COLLATE 'latin1_swedish_ci',
	`uitslag` BIGINT(20) NOT NULL
) ENGINE=MyISAM;


-- Structuur van  tabel mbvolley.w_team wordt geschreven
DROP TABLE IF EXISTS `w_team`;
CREATE TABLE IF NOT EXISTS `w_team` (
  `wedstrijd` int(11) NOT NULL,
  `team` int(11) NOT NULL,
  `rol` int(11) NOT NULL,
  `strafpunten` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`wedstrijd`,`team`),
  KEY `fk_WEDSTRIJD_has_team_team1_idx` (`team`),
  KEY `fk_WEDSTRIJD_has_team_WEDSTRIJD1_idx` (`wedstrijd`),
  CONSTRAINT `fk_WEDSTRIJD_has_team_team1` FOREIGN KEY (`team`) REFERENCES `team` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_WEDSTRIJD_has_team_WEDSTRIJD1` FOREIGN KEY (`wedstrijd`) REFERENCES `wedstrijd` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel mbvolley.w_team: ~9 rows (ongeveer)
/*!40000 ALTER TABLE `w_team` DISABLE KEYS */;
INSERT INTO `w_team` (`wedstrijd`, `team`, `rol`, `strafpunten`) VALUES
	(1, 7, 1, 0),
	(1, 8, 2, 0),
	(1, 9, 3, 0),
	(2, 1, 1, 0),
	(2, 2, 2, 0),
	(2, 3, 3, 0),
	(19, 7, 3, 8),
	(19, 8, 1, 0),
	(19, 9, 2, 0);
/*!40000 ALTER TABLE `w_team` ENABLE KEYS */;


-- Structuur van  view mbvolley.statsview wordt geschreven
DROP VIEW IF EXISTS `statsview`;
-- Tijdelijke tabel wordt verwijderd, en definitieve VIEW wordt aangemaakt.
DROP TABLE IF EXISTS `statsview`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `statsview` AS SELECT T.klasse, U.team, T.naam, COUNT(DISTINCT U.wedstrijd) W, SUM(U.punten)-T.strp P, 
       SUM(U.score) Sv, SUM(UT.score) St, 
		 SUM(U.score)-SUM(UT.score) S,
		 T.strp
FROM   UITSLAG_SET_TEAM U, UITSLAG_SET_TEAM UT, W_TEAM WT, TEAM_STRP T
WHERE  U.wedstrijd = UT.wedstrijd
AND    U.set = UT.set
AND    U.team <> UT.team
AND    WT.wedstrijd = U.wedstrijd AND WT.team = U.team
AND    T.id = U.team
GROUP BY U.team
ORDER BY T.klasse, P DESC, W ASC, S DESC ;


-- Structuur van  view mbvolley.team_strp wordt geschreven
DROP VIEW IF EXISTS `team_strp`;
-- Tijdelijke tabel wordt verwijderd, en definitieve VIEW wordt aangemaakt.
DROP TABLE IF EXISTS `team_strp`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `team_strp` AS SELECT T.*, SUM(WT.strafpunten) Strp
FROM   TEAM T, W_TEAM WT
WHERE  T.id = WT.team
GROUP BY T.id
UNION
SELECT T.*, 0 Strp
FROM TEAM T
WHERE T.id NOT IN (
  SELECT DISTINCT team
  FROM W_TEAM
)
ORDER BY id ;


-- Structuur van  view mbvolley.team_uitslag_view wordt geschreven
DROP VIEW IF EXISTS `team_uitslag_view`;
-- Tijdelijke tabel wordt verwijderd, en definitieve VIEW wordt aangemaakt.
DROP TABLE IF EXISTS `team_uitslag_view`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `team_uitslag_view` AS SELECT *
FROM TEAM_STRP T ;


-- Structuur van  view mbvolley.wedstrijdschema_view wordt geschreven
DROP VIEW IF EXISTS `wedstrijdschema_view`;
-- Tijdelijke tabel wordt verwijderd, en definitieve VIEW wordt aangemaakt.
DROP TABLE IF EXISTS `wedstrijdschema_view`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `wedstrijdschema_view` AS SELECT * FROM wedstrijd_view 
UNION SELECT W.id, W.speelweek, S.datum, W.tijd, W.veld, '' klasse, 
             0 taid, '' teama, 0 tbid, '' teamb, '' teams, '' klasse2, -1 uitslag
FROM WEDSTRIJD W, SPEELWEEK S 
WHERE W.speelweek = S.id
AND   W.id NOT IN (SELECT id FROM wedstrijd_view)
ORDER BY speelweek, tijd, veld ;


-- Structuur van  view mbvolley.wedstrijd_view wordt geschreven
DROP VIEW IF EXISTS `wedstrijd_view`;
-- Tijdelijke tabel wordt verwijderd, en definitieve VIEW wordt aangemaakt.
DROP TABLE IF EXISTS `wedstrijd_view`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `wedstrijd_view` AS SELECT W.id, W.speelweek, S.datum, W.tijd, W.veld, TA.klasse, TA.id AS taid, TA.naam AS teama, TB.id AS tbid, TB.naam AS teamb, TS.naam AS teams, TS.klasse AS klasse2, 1 uitslag
FROM WEDSTRIJD W, SPEELWEEK S, W_TEAM WTA, TEAM TA, W_TEAM WTB, TEAM TB, W_TEAM WTS, TEAM TS
WHERE W.speelweek = S.id
  AND W.id = WTA.wedstrijd AND WTA.team = TA.id AND WTA.rol = 1 
  AND W.id = WTB.wedstrijd AND WTB.team = TB.id AND WTB.rol = 2
  AND W.id = WTS.wedstrijd AND WTS.team = TS.id AND WTS.rol = 3 
  AND W.id IN (
    SELECT DISTINCT wedstrijd
    FROM UITSLAG_SET_TEAM)
UNION
SELECT W.id, W.speelweek, S.datum, W.tijd, W.veld, TA.klasse, TA.id AS taid, TA.naam AS teama, TB.id AS tbid, TB.naam AS teamb, TS.naam, TS.klasse AS klasse2, 0 uitslag
FROM WEDSTRIJD W, SPEELWEEK S, W_TEAM WTA, TEAM TA, W_TEAM WTB, TEAM TB, W_TEAM WTS, TEAM TS
WHERE W.speelweek = S.id
  AND W.id = WTA.wedstrijd AND WTA.team = TA.id AND WTA.rol = 1 
  AND W.id = WTB.wedstrijd AND WTB.team = TB.id AND WTB.rol = 2
  AND W.id = WTS.wedstrijd AND WTS.team = TS.id AND WTS.rol = 3 
  AND W.id NOT IN (
    SELECT DISTINCT wedstrijd
    FROM UITSLAG_SET_TEAM)
ORDER BY speelweek ASC,tijd ASC, veld ASC ;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
