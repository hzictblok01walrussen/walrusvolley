<?php
// Sessions, Security and Authorization
include ('security.php');

	// controleer of uitloggen
	if (isset($_GET['action']) && $_GET['action']==='logoff') {
		session_destroy();
		//Een header sturen
		header('Location: index.php');
		exit;
	}

	$loginTry = isset($_POST['userid']) && isset($_POST['passwd']);
	// Controleer op inloggen 
	if ($loginTry) {
		include_once('userstorage.php');
		$username = strip_tags($_POST['userid']);
		$passwd = strip_tags($_POST['passwd']);
		// Het wachtwoord in de database is gehasht, deze moeten we uitlezen om het te vergelijken.
		$options = ['salt' => ';sdlfkj;sdkjasfkjasd;kfaj;fkasj;'];
		$hash = password_hash($passwd, PASSWORD_DEFAULT, $options);
		
		if (authenticateUser($username, $hash)) {
			//Een header sturen
			header('Location: index.php');
			exit;
		}
	}

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>MBV Volley</title>
		<?php include 'head.html' ?>		
	</head>
	<body>
		<?php include 'header.php' ?>
		<main class="container">	
			<?php
			if ($loginTry && !isAuthenticated()) {
				echo '<span class="error">Login niet succesvol. Probeer opnieuw</span>';
			}
			?>
			<form method="post" action="login.php" style="width: 450px; margin-left: auto; margin-right: auto">
				<table >
					<tr>
						 <td valign="top">
						  	<label for="userid">Gebruikersnaam:</label>
						 </td>
						 <td valign="top">
						  	<input  type="text" name="userid" maxlength="50" size="30">
						 </td>
					</tr>
					<tr>
						 <td valign="top">
						  	<label for="passwd">Wachtwoord:</label>
						 </td>
						 <td valign="top">
						  	<input  type="password" name="passwd" value="" maxlength="50" size="30">
						 </td>
					</tr>
					<tr>
						 <td valign="top"></td>
						 <td valign="top">
						  	<input type="submit" name="commit" value="Login">
						 </td>
					</tr>
				</table>	
			</form>
		</main>
	</body>
</html>
