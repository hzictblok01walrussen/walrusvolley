<?php
// Sessions, Security and Authorization
include ('security.php');
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>MBVolley - Home</title>
		<?php include 'head.html' ?>		
	</head>
	<body>
		<?php include 'header.php' ?>
		<main class="container">
			<div class="jumbotron">	
				<h1>Welkom</h1>
				<p><i class="fa fa-quote-left fa-3x pull-left fa-border"></i>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed rutrum porta mi, at laoreet justo 
					pharetra vel. Proin at risus id nibh molestie feugiat. Phasellus felis nunc, aliquam non 
					tristique in, pharetra a tortor. Quisque ut dapibus tellus. Maecenas non risus bibendum, 
					hendrerit leo sed, hendrerit tortor. Cras non risus mollis, tempus arcu nec, volutpat lorem. 
					Nam eget sollicitudin est. Proin euismod volutpat risus id consequat. Phasellus id nunc ex. 
					Nulla massa nisi, dignissim ac metus et, ultricies rhoncus ex. Fusce eget velit a metus 
					vehicula ornare.
				</p>
				<p>Quisque porttitor magna viverra est lobortis scelerisque. Pellentesque nec ligula vitae libero 
					mollis efficitur. Quisque quis dui non neque dapibus egestas. Sed blandit nulla felis. Phasellus 
					dignissim metus sit amet dui cursus tincidunt. Sed hendrerit magna vitae libero lobortis, sed 
					commodo velit rutrum. Proin luctus imperdiet mauris, ut dapibus enim volutpat nec. Suspendisse 
					dapibus fermentum sem vitae imperdiet.
				</p>	
			</div>	
		</main>
	</body>
</html>
 