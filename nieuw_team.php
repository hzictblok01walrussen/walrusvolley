<?php
// Sessions, Security and Authorization
include ('security.php');

//var_dump($_POST);
//Verbinding maken met de database
require_once 'db.php';
$mysqli = connectDB();
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>MBV Volley</title>
		<?php include 'head.html'
		?>
	</head>
	<body>
		<?php include 'header.php'
		?>
		<main class="container">
			<?php
			// Externe scripts includen
			include_once ('formvalidationtools.php');
			// Controleer of form wordt opgevraagd of ingestuurd
			if (isset($_POST['send'])) {
				// Form ingestuurd: Verwerken maar

				// Als eerste stap controleren en valideren we alle data uit het form
				if (!isset($_POST['klas']) || !isset($_POST['teamnaam'])) {
					printErrorAndDie('Het lijkt er op dat het formulier dat u gebruikt niet klopt.');
				}

				// Overnemen van de dat uit het form naar lokale variabelen
				$klas = strip_tags($_POST['klas']);
				$teamnaam = strip_tags($_POST['teamnaam']);

				// Valideren van de input, met behulp van extern script met functies
				// error_message wordt gevuld als er foutberichten zijn
				$error_message = "";

				$error_message .= validateCharacters($teamnaam, 2, 'Deze naam is niet valide.');

				// Er is iets mis als de lengte van error_message > 0
				if (strlen($error_message) > 0) {
					printErrorAndDie($error_message);
				}
				// De input is nu goed, dus kan het worden verwerkt
				echo $sql = "INSERT INTO team (naam, klasse) VALUES ('" . $teamnaam . "','" . $klas . "')";
				$result = $mysqli -> query($sql);
				//laat de laatste error code zien
				//echo $mysqli->error;

				//Een header sturen
				header('Location: index.php');
				exit ;
			} else {
			}
			// Het form laten zien...
			?>
			<form  class="form-horizontal" action="" method="POST" role="form">
				<div class="panel panel-default">
					<div class="panel-body">
						<table width="850px">
							<tr>
								<td><label for="klas">klasse</label></td>
								<td>
								<select id="klas" name="klas" style="width: 185pt;">
									<option value="" selected="selected"></option>
									<option value="D1">Dames 1</option>
									<option value="D2">Dames 2</option>
									<option value="H1">Heren 1</option>
									<option value="H2">Heren 2</option>
									<option value="H3">Heren 3</option>
								</td></select>
							</tr>
							<tr>
								<td><label for="teamnaam">teamnaam</label></td>
								<td>
								<input type="text" id="test" name="teamnaam" maxlength="50" size="30">
								</td>
							</tr>

						</table>
					</div>
					<div class="panel-footer">
						<button type="submit" name="send" class="btn btn-primary">
							Verzenden
						</button>
					</div>
				</div>
			</form>

		</main>
		<script src="lib/jquery/jquery.min.js"></script>
		<!-- link naar het javascript bestand dat de validatie uitvoert -->
		<script src="js/validate.js"></script>
	</body>
</html>